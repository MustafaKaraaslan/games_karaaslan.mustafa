package at.karaaslan.games.wintergame;

import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {
	private List<Actor> Actors;

	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		for (Actor actor : this.Actors) {
			actor.render(graphics);
		}

	}

	@Override
	public void init(GameContainer gc) throws SlickException {
		// 1 mal aufgerufen
		this.Actors = new ArrayList<>();
		this.Actors.add(new OvalActor(0, 400));
		this.Actors.add(new CircleActor(375, 0));
		this.Actors.add(new RectangleActor(100, 100));

		for (int i = 0; i < 50; i++) {
			this.Actors.add(new Snowflake(Snowflake.size.large));
			this.Actors.add(new Snowflake(Snowflake.size.medium));
			this.Actors.add(new Snowflake(Snowflake.size.small));
		}
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta = zeit seit dem letzten aufruf
		for (Actor actor : this.Actors) {
			actor.update(gc, delta);
		}

	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Wintergame"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

	public void addActor(Actor actor) {
		this.Actors.add(actor);
	}

}
