package at.karaaslan.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class CircleActor implements Actor  {
	private double x,y;
	private boolean circleDown = true;
	private boolean circleUp = false;
	
	
	public CircleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public void update(GameContainer gc, int delta) {
		
		if(this.circleDown == true) {
			this.y++;
			if(this.y >= 600) {
				this.circleUp = true;
				this.circleDown = false;
			}
		}
		
		else if(this.circleUp == true) {
			this.y = 0;
			this.circleUp = false;
			this.circleDown = true;
		}

	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 50, 50);
	}

}
