package at.karaaslan.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectangleActor implements Actor {
	private double x,y;
	private boolean rectRight = true;
	private boolean rectLeft = false;
	private boolean rectUp = false;
	private boolean rectDown = false;
	
	
	
	public RectangleActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public void update(GameContainer gc, int delta) {
		
		if(this.rectRight ==  true) {
			this.x++;
			if (this.x >= 700) {
				this.rectDown = true;
				this.rectRight = false;
			}
		}
		
		
		else if(this.rectDown == true) {
			this.y++;
			if (this.y >= 500) {
				this.rectLeft = true;
				this.rectDown = false;
			}
		}
		
		else if(this.rectLeft == true) {
			this.x--;
			if(this.x <= 100) {
				this.rectLeft = false;
				this.rectUp = true;
			}
		}
		
		else if(this.rectUp == true) {
			this.y--;
			if(this.y <= 100) {
				this.rectUp = false;
				this.rectRight = true;
			}
		}
		
	}
	
	public void render(Graphics graphics) {
		graphics.drawRect((float)this.x, (float)this.y, 50, 50);
	}

}
