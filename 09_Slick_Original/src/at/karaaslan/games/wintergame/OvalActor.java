package at.karaaslan.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor implements Actor  {
	private double x,y;
	private boolean ovalRight = true;
	private boolean ovalLeft = false;
	
	
	public OvalActor(double x, double y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	
	public void update(GameContainer gc, int delta) {
		
		if(this.ovalRight == true) {
			this.x++;
			if(this.x >= 700) {
				this.ovalRight = false;
				this.ovalLeft = true;
			}
		}
		
		else if(this.ovalLeft == true) {
			this.x--;
			if(this.x <= 0) {
				this.ovalLeft = false;
				this.ovalRight = true;
			}
		}
	}
	
	public void render(Graphics graphics) {
		graphics.drawOval((float)this.x, (float)this.y, 100, 50);
	}

}
