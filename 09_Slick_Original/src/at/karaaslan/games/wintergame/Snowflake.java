package at.karaaslan.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import java.util.Random;

public class Snowflake implements Actor {
	private float x,y;
	
	private float height, speed, width;
	private Random random;
	private size siz;
	public enum size {
		small, medium, large
	}

	
	
	public Snowflake(size siz) {
		super();
		this.random = new Random();
		setRandomPosition();
		this.y *= -1;
		this.siz = siz;
		
		
		if(this.siz == size.small) {
			this.width = 10;
			this.height = 10;
			this.speed = 0.25f;
		}
		
		if(this.siz == size.medium) {
			this.width = 15;
			this.height = 15;
			this.speed = 0.4f;
		}
		
		if(this.siz == size.large) {
			this.width = 25;
			this.height = 25;
			this.speed = 0.6f;
		}
		
		
	}


	private void setRandomPosition() {
		this.x = random.nextInt(799) + 1;
		this.y = random.nextInt(600) + 0;
	}

	
	public void update(GameContainer gc, int delta) {
		if(this.y <= 600) {
			this.y += delta * this.speed;
			if(this.y >= 600) {
				setRandomPosition();
				this.y = -40;
			}
		}
	}
	
	public void render(Graphics graphics) {
		graphics.fillOval(this.x, this.y, this.width, this.height );
	}

}
